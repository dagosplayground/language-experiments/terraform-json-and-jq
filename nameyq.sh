#! /bin/bash

get_namespaces() { 
  yq ".namespaces[].namespace_appshortname" namespaces.yaml -r
}

names=$(get_namespaces)

for i in ${names[@]}; do 
  echo $i
done

