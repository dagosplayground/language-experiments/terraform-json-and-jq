# Object Pull

The goal is use `jq` to pull data and create a simple object array of the output. This a local mock up a free'er and better system than in a deployment pipeline.

## Example

**To Run:** `./selector.sh`

**Output:**
```json
{
  "TACO": "1.2.0",
  "CHALUPA": "3.1.0"
}
```
