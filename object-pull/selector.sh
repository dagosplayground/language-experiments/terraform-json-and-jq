#!/bin/bash

jq --arg mytype "ECS" \
  '.COMPONENTS[]?|select(.TYPE==$mytype)| {(.BRANCH) : .VERSION } ' rdata.json \
  | jq -s '. | add'

