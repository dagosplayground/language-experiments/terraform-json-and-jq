#!/bin/bash

get_namespaces() {
  fileread=$1
  jq '.outputs.localout.value[]?.namespace_appshortname' $fileread -r
}

get_namespaces_other() {
  fileread=$1
  shortname=$2
  fieldname=$3
  jq --arg shortname "$shortname" \
    --arg keyname "$other" \
    --arg fieldname "$fieldname" \
    '.outputs.localout.value[]? 
    | select(.namespace_appshortname==$shortname)
    | .[$fieldname]' \
    $fileread -r
}

terraform apply --auto-approve --json > output.json

namesapce_file=output.json

names=$(get_namespaces output.json)
for i in ${names[@]}; do
  echo $i
done

echo "###### break ######"

get_namespaces_other output.json ftv registration_token
get_namespaces_other output.json guu registration_token
get_namespaces_other output.json nfr registration_token

echo "###### tags ######"
get_namespaces_other output.json ftv eks_tags 
get_namespaces_other output.json guu eks_tags 
get_namespaces_other output.json nfr eks_tags 

echo "##### helm ######"
guu_helm=$(get_namespaces_other output.json guu helm_uninstall)
ftv_helm=$(get_namespaces_other output.json ftv helm_uninstall)

if [ "$guu_helm" == "true" ]; then
  echo "Guu wants helm gone"
fi

if [ "$ftv_helm" == "true" ]; then
  echo "Ftv wants helm gone"
fi

if [ "$nfr_helm" == "true" ]; then
  echo "Nfr wants helm gone"
fi
