
namespaces = [
  {
    namespace_identifier   = "gitlabe-runner"
    namespace_appshortname = "guu"
    service_account_name   = "gitlab-runner"
    irsa_enabled           = false
    irsa_type_role         = "gitlab-runner"
    registration_token     = "1234"
    eks_tags = [
      "eks",
      "val1"
    ]
    helm_uninstall = true
  },
  {
    namespace_identifier   = "gitlabe-runner"
    namespace_appshortname = "nfr"
    service_account_name   = "gitlab-runner"
    irsa_enabled           = true
    irsa_type_role         = "gitlab-runner"
    registration_token     = "5678"
    eks_tags = [
      "eks",
      "val2"
    ]
  },
  {
    namespace_identifier   = "gitlabe-runner"
    namespace_appshortname = "ftv"
    service_account_name   = "gitlab-runner3"
    irsa_enabled           = true
    irsa_type_role         = "gitlab-runner"
    registration_token     = "2468"
    eks_tags = [
      "eks",
      "val3"
    ]
  }
]
