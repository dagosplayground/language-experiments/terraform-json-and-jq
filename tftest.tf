

# output "tfyaml" {
#   value = tolist(yamldecode(file("namespaces.yaml")).namespaces)
# }

# output "vartoyaml" {
#   value = yamlencode(local.namespaces)
# }

output "localout" {
  value = local.namespaces
}

output "ismatch" {
  value = local.namespaces == yamldecode(file("namespaces.yaml")).namespaces ? "true" : "false"
}

output "loopout" {
  value = [for namespace in local.namespaces : namespace
  if namespace.irsa_enabled == true]
}

output "eksout" {
  value = [for namespace in local.namespaces : {
    name = namespace.namespace_appshortname
    tags = namespace.eks_tags
    }
  if contains(namespace.eks_tags, "val1")]
}

locals {
  namespaces = var.namespaces
  # namespaces = [
  #   {
  #     namespace_identifier   = "gitlabe-runner"
  #     namespace_appshortname = "guu"
  #     service_account_name   = "gitlab-runner"
  #     irsa_enabled           = true
  #     irsa_type_role         = "gitlab-runner"
  #   },
  #   {
  #     namespace_identifier   = "gitlabe-runner"
  #     namespace_appshortname = "nfr"
  #     service_account_name   = "gitlab-runner"
  #     irsa_enabled           = true
  #     irsa_type_role         = "gitlab-runner"
  #   },
  #   {
  #     namespace_identifier   = "gitlabe-runner"
  #     namespace_appshortname = "ftv"
  #     service_account_name   = "gitlab-runner"
  #     irsa_enabled           = true
  #     irsa_type_role         = "gitlab-runner"
  #   }
  # ]
}

